import styled from 'styled-components'

const Input = styled.input`
  width: 80%;
  height: 13px;
  border-radius: 24px;
  border: 1px solid #DEDEDE;
  padding: 13px 0 12px 15px;
  background-color: transparent;
  color: #fff;
  font-weight: medium;
  margin-bottom: 12px;
  ::placeholder,
  ::-webkit-input-placeholder {
    color: #fff;
  }
  :-ms-input-placeholder {
     color: #fff;
  }
`

export default Input
