import styled from 'styled-components'

const Button = styled.button`
    height: 38px;
    width: 160px;
    color: #fff;
    background-color: #2398FF;
    text-align: center;
    border: none;
    border-radius: 24px;
`

export default Button
