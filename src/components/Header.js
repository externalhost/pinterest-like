import styled from 'styled-components'
import React from 'react'


import AvatarProfile from '../assets/avatar.png'
import ProfileSpan from './ProfileSpan'
import Logo from './Logo'

const HeaderTemplate = styled.div`
    height: 80px;
    width: 100%;
    color: #000;
    text-align: center;
    border-bottom: 1px solid #DEDEDE;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
`

const Header = props => {
    return (
        <HeaderTemplate>
            <span>Pinterest</span>
            <ProfileSpan>
                <Logo src={AvatarProfile} height="25" />
                {props.username || 'name'}
            </ProfileSpan>
        </HeaderTemplate>
    )
}

export default Header;