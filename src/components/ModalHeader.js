import styled from 'styled-components'
import React, { useEffect, useState } from 'react'

import Heart from './Heart'

import { addToFavorite, checkFavorite, delFromFavorite } from '../utils/http'

const ModalHeaderTemplate = styled.div`
    width: 100%;
    color: #000;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: space-around;
`

const ModalHeader = props => {
    const [favorite, setFavorite] = useState(false);
    const [response, setResponse] = useState(false);
    useEffect(() => {
        checkFavorite(props.image.id).then(data =>
            data.length > 0 && setFavorite(true)
        )
    }, [response]);

    const favoriteAdd = () => {
        addToFavorite(props.image).then(() => {
            setResponse(!response)
        });
    }

    const favoriteDel = () => {
        delFromFavorite(props.image.id).then(() => {
            setResponse(!response)
        });
    }
    return (
        <ModalHeaderTemplate>
            <p>{props.image.name || "Sans titre :("}</p>
            <Heart height="29px" color={favorite ? '#FF8F9A' : '#858585'} onClick={() => !favorite ? favoriteAdd() : favoriteDel()} />
        </ModalHeaderTemplate>
    )
}

export default ModalHeader;