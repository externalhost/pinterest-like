import styled from 'styled-components'
import React from 'react'
import { navigate } from '@reach/router'

import MyLogo from '../assets/logo.png'

import Logo from './Logo'
import Heart from './Heart'

const NavBarTemplate = styled.div`
    height: 80px;
    width: 100%;
    color: #000;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: space-around;
    border-top: 1px solid #DEDEDE;
    position: fixed;
    bottom: 0;
    z-index: 999;
    background-color: #fff;
`

const NavBar = props => {
    return (
        <NavBarTemplate>
            <Logo src={MyLogo} height="52" onClick={() =>  navigate('/') }/>
            <Heart onClick={() => navigate('/favorite') } color={props.color} />
        </NavBarTemplate>
    )
}

export default NavBar;