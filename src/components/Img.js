import styled from 'styled-components'

const Img = styled.img`
  border-radius: 24px;
  width: 100%;
  height: auto;
  color: #fff;
`

export default Img
