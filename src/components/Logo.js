import styled from 'styled-components'

const Logo = styled.img`
  width: auto;
  height: ${props => (props.height ? props.height + 'px' : '144px')};
  border-radius: 100%;
`
export default Logo
