import styled from 'styled-components'
import React from 'react'

import CloseLogo from '../assets/cancel.png'
import Logo from './Logo'
import Img from './Img'

import ModalHeader from './ModalHeader'

const ModalTemplate = styled.div`
    height: 90vh;
    width: 100%;
    color: #000;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: space-around;
    flex-direction: column;
    border-top: 1px solid #DEDEDE;
    position: absolute;
    bottom: 0;
    z-index: 9999;
    background-color: #fff;
`
const Close = styled(Logo)`
    position: absolute;
    top: 10px;
    right: 10px;
`

const Modal = props => {
    return (
        <ModalTemplate>
            <Close src={CloseLogo} height="20" onClick={() => props.triggerModal()}/>
            <ModalHeader image={props.image} />
            <Img src={props.image.image} />
        </ModalTemplate>
    )
}

export default Modal;