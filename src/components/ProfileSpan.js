import styled from 'styled-components'

const ProfileSpan = styled.div`
    height: 35px;
    width: 80px;
    padding: 0 5px 0 5px;
    color: #FFF;
    background-color: #FF8F9A;
    text-align: center;
    border-top-left-radius: 30px;
    border-bottom-left-radius: 30px;
    font-size: 9px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
    position: absolute;
`

export default ProfileSpan;