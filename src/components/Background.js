import styled from 'styled-components'

const Background = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${props => (props.color)};
`

export default Background
