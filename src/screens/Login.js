import React from 'react';

import Background from '../components/Background'
import Logo from '../components/Logo'
import Input from '../components/Input'
import Button from '../components/Button'

import srcLogo from '../assets/logo.png'
const Login = () => {
    return (
        <>
            <Background color="#FF8F9A" className="login__page">
                <Logo src={srcLogo} />
                <div className="form">
                    <Input placeholder="Email"/>
                    <Input placeholder="Password"/>
                </div>
                <Button>Login</Button>
            </Background>
        </>
    );
};

export default Login;