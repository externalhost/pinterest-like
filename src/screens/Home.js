import React, {useEffect, useState} from 'react';
import StackGrid from "react-stack-grid";

import Background from '../components/Background'
import Header from '../components/Header'
import Img from '../components/Img'

import Modal from '../components/Modal'
import NavBar from '../components/NavBar'

import {getImagesList} from '../utils/http'
const Home = () => {
    const [images, setImages] = useState([]);
    const [renderHook, setRenderHook] = useState();
    const [imgToShow, setImgToShow] = useState({});
    const [modalShow, setModalShow] = useState(false);

    useEffect(() => {
        getImagesList().then(data => {
            setImages(data)
        });
        setTimeout(() => {
          setRenderHook(null);
        }, 1000);
    }, []);

    const triggerModal = image => {
        setModalShow(!modalShow);
        setImgToShow(image)
    }

    return (
        <>
            <Background color="#FFF" className={modalShow ? "home__page noScroll" : "home__page"}>
                {
                    modalShow && <Modal triggerModal={triggerModal} image={imgToShow} />
                }
                <Header username="Hellow" />
                 <StackGrid columnWidth={'50%'} horizontal={true} >
                    {
                        images && images.map(image => {
                            return <Img src={image.image} key={image.id} onClick={() => triggerModal(image)}/>
                        })
                    }
                 </StackGrid>
                 <NavBar />
            </Background>
        </>
    );
};

export default Home;