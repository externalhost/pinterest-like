import React, {useEffect, useState} from 'react';
import StackGrid from "react-stack-grid";

import Background from '../components/Background'
import Header from '../components/Header'
import Img from '../components/Img'

import Modal from '../components/Modal'
import NavBar from '../components/NavBar'

import {getFavorite} from '../utils/http'
const Favorite = () => {
    const [images, setImages] = useState([]);
    const [renderHook, setRenderHook] = useState();
    const [imgToShow, setImgToShow] = useState({});
    const [modalShow, setModalShow] = useState(false);

    useEffect(() => {
        getFavorite().then(data => {
            setImages(data)
        });
        setTimeout(() => {
          setRenderHook(null);
        }, 1000);
    }, []);

    const triggerModal = image => {
        setModalShow(!modalShow);
        setImgToShow(image)
    }

    return (
        <>
            <Background color="#FFF" className={modalShow ? "favorite__page noScroll" : "favorite__page"}>
                {
                    modalShow && <Modal triggerModal={triggerModal} image={imgToShow} />
                }
                <Header username="Hellow" />
                 {images && images.length > 0 ? (<StackGrid columnWidth={'50%'} horizontal={true} >
                    {
                        images.map(image => {
                            return <Img src={image.image} key={image.id} onClick={() => triggerModal(image)}/>
                        })
                    }
                 </StackGrid>) : <p>Pas de favoris :'(</p>}
                 <NavBar color="#FF8F9A" />
            </Background>
        </>
    );
};

export default Favorite;