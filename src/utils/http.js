import Unsplash, { toJson } from 'unsplash-js';

const unsplash = new Unsplash({ accessKey: '-LWbPCSwXp0hV6dHIZRfX6_zuZ0d_1yl0o8_4ncchiE' });


export const Login = async (email, password) => {
    return (email === 'mexi@cain.fr' && password === 'test')
}

export const getImagesList = async () => {
    const data = await unsplash.photos.listPhotos(0, 20, "latest").then(toJson);
    return data.map(el => {
        return {
            id: el.id,
            name: el.alt_description,
            image: el.urls.regular
        }
    })
}

export const addToFavorite = async (image) => {
    let favorites = await localStorage.getItem('favorite');
    if (favorites) {
        favorites = JSON.parse(favorites);
        favorites.push(image);
        localStorage.setItem('favorite', JSON.stringify(favorites));
        return 'success';
    } else {
        localStorage.setItem('favorite', JSON.stringify([image]));
        return 'success';
    }
}

export const getFavorite = async () => {
    const favorites = await localStorage.getItem('favorite');
    if (favorites) {
        return JSON.parse(favorites);
    } else {
        return [];
    }
}

export const delFromFavorite = async (id) => {
    const favorite = await getFavorite();
    let newFavorite = []
    favorite.map(el => el.id !== id && newFavorite.push(el));
    localStorage.setItem('favorite', JSON.stringify(newFavorite));
    return newFavorite;
}


export const checkFavorite = async (id) => {
    const favorite = await getFavorite();
    return favorite.filter(el => el.id === id);
}