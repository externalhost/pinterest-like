import React, { useEffect, useState } from 'react'
import { Router, Redirect, navigate } from '@reach/router'

import './App.css'

const Home = React.lazy(() => import('./screens/Home'))
const Login = React.lazy(() => import('./screens/Login'))
const Favorite = React.lazy(() => import('./screens/Favorite'))

function App() {
  // const [isTokenExist, setTokenExist] = useState(false)

  // useEffect(() => {
  //   if (
  //     localStorage.getItem('token') &&
  //     localStorage.getItem('token').length > 0
  //   ) {
  //     setTokenExist(true)
  //     navigate('/')
  //   }
  // }, [isTokenExist])

  return (
    <div className='App'>
      <React.Suspense fallback='Attendre svp..'>
        <Router>
          {/* <Login path='/login' />
          {isTokenExist ? (
            <>
              
            </>
          ) : (
            <Redirect from='*' to='/login' noThrow />
          )} */}
          <Home path='/' />
          <Login path='/login' />
          <Favorite path='/favorite' />
        </Router>
      </React.Suspense>
    </div>
  )
}

export default App